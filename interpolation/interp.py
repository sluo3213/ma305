#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Template for Python file, Jim Hefferon.
"""
__version__ = "0.0.1"
__author__ = "Jim Hefferon"
__license__ = "GPL 3.0"

import sys
import os, os.path
import re, string
import traceback, pprint
import argparse
import time
import numpy

# Global variables spare me from putting them in the call of each fcn.
VERBOSE = False
DEBUG = False

PGM_ROOTNAME = os.path.splitext(os.path.basename(sys.argv[0]))[0]
PGM_SRC_DIR = os.path.dirname(__file__)

class JHException(Exception):
    pass

import logging
# Potential logging levels: DEBUG | INFO | WARNING | ERROR | CRITICAL
LOG_LEVEL_CHOICES=["debug", "info", "warning", "error", "critical", "default"]
DEFAULT_LOG_LEVEL = "warning"
if not(DEFAULT_LOG_LEVEL in LOG_LEVEL_CHOICES):
    critical("DEFAULT_LOG_LEVEL "+str(DEFAULT_LOG_LEVEL)+ \
             " must be in LOG_LEVEL_CHOICES="+str(LOG_LEVEL_CHOICES))

def _set_log_level(log, choice=DEFAULT_LOG_LEVEL):
    c = choice.casefold()  # like lower() but for case-insensitive matching
    # log.debug('Logging level set to '+choice)
    if (c == "debug"):
        log.setLevel(logging.DEBUG)
    elif (c == "info"):
        log.setLevel(logging.INFO)
    elif (c == "warning"):
        log.setLevel(logging.WARNING)
    elif (c == "error"):
        log.setLevel(logging.ERROR)
    elif (c == "critical"):
        log.setLevel(logging.CRITICAL)
    else:
        error("Logging level {0!s} not known".format(choice))

# Establish logging
log = logging.getLogger(__name__)
_set_log_level(log)
# Log errors to the console
log_sh = logging.StreamHandler(stream=sys.stderr)
log_sh.setFormatter(logging.Formatter('%(levelname)s - Line: %(lineno)d\n  %(message)s'))
_set_log_level(log_sh,"ERROR")
log.addHandler(log_sh)
# Log most everything to a file
log_fh = logging.FileHandler(os.path.abspath(os.path.join(
    os.path.dirname(__file__), 
    os.path.basename(__file__).rstrip('.py') + '.log')),
                         mode='w')
log_fh.setFormatter(logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - Line: %(lineno)d\n%(message)s'))
_set_log_level(log_fh,"INFO")
log.addHandler(log_fh)
if DEBUG:
    _set_log_level(log_sh,"DEBUG")
    _set_log_level(log_fh,"DEBUG")

def warning(s):
    t = 'WARNING: '+s+"\n"
    log.warning(t)

def error(s, level=10):
    t = 'ERROR: '+s+"\n"
    log.error(t,exc_info=True)
    sys.exit(level)

def critical(s, level=1):
    t = 'CRITICAL ERROR: '+s+"\n"
    log.critical(t,exc_info=True)
    sys.exit(level)


# ===========================================================
def check_dup_first_entries(list_of_pts):
    """
    check if there are dulpicated x values in list_of_pts
    """
    first = list_of_pts[0]
    for next in list_of_pts[1:]:
        if(first[0] == next[0]):
            return False
        first = next
    return True



def p_linear(list_of_pts, x):
    """
    find piece wise linear interpolation at x list_of_pts
    list of pairs of numbers
    Must be ordered by first item
    x -- number evaluate interpolation here
    """
    list_of_pts.sort()

    if not(check_dup_first_entries(list_of_pts)):
        return None
    
    dex = 0
    left_pt = list_of_pts[dex]
    x1, y1 = left_pt
    dex = dex + 1
    right_pt = list_of_pts[dex]
    x2, y2 = right_pt
    dex = dex + 1
    while not((x1<=x) and (x<x2)):
        left_pt = right_pt
        if(dex >= len(list_of_pts)):
            return None
        right_pt = list_of_pts[dex]
        x1, y1 = left_pt
        x2, y2 = right_pt
        dex += 1
    

    output = ((y2 - y1)/(x2 - x1))*(x - x1) + y1
    return output

def p_quad(list_of_pts, x):
    """
    find piecewise quadratic interpolation at x list_of_pts
    """
    list_of_pts.sort()

    if not(check_dup_first_entries(list_of_pts)):
        return None



    array_size = 3*(len(list_of_pts)-1)
    coeff_mat = np.zeros((array_size,array_size))
    const_mat = np.zeros(array_size)
    #Do the functions one at a time

    
    row_num = 0
    right_pt = list_of_pts[0]
    for i in range(1,len(list_of_pts)):
        left_pt = right_pt
        x1, y1 = left_pt
        right_pt = list_of_pts[i]
        x2,y2 = right_pt
        coeff_mat[row_num,0] = 1
        coeff_mat[row_num,1] = 0
        coeff_mat[row_num,2] = 0
        row_num += 1
        coeff_mat[row_num,0] = 1
        coeff_mat[row_num,1] = x2-x1
        coeff_mat[row_num,2] = (x2-x1)**2
        row_num += 1
        coeff_mat[row_num,3*(i-1)] = 0
        coeff_mat[row_num,3*(i-1)+1] = 1
        coeff_mat[row_num,3*(i-1)+2] = 2*(x2-x1)
        coeff_mat[row_num,3*(i-1)+3] = 0
        coeff_mat[row_num,3*(i-1)+4] = -1
        coeff_mat[row_num,3*(i-1)+5] = -2*(x2-x1)
        row_num += 1
    coeff_mat[row_num,0] = 0
    coeff_mat[row_num,1] = 0
    coeff_mat[row_num,2] = 2










def main(args):
    list_of_pts = [(3,2),(3,4),(5,6)]
    y = (p_linear(list_of_pts, 2))
    print('y =', y)
    



# ===========================================================
if __name__ == '__main__':
    try:
        start_time = time.time()
        # Parser: See http://docs.python.org/dev/library/argparse.html
        parser = argparse.ArgumentParser(description=__doc__+
                                         "  Author: "+__author__
                                         +", Version: "+__version__
                                         +", License: "+__license__)
        parser.add_argument('-D', '--debug',
                            action='store_true',
                            default=DEBUG,
                            help="Run debugging code. Default: {0!s}".format(DEBUG))
        parser.add_argument('-L', '--log_level',
                            action='store',
                            type=str,
                            choices=LOG_LEVEL_CHOICES,
                            default=DEFAULT_LOG_LEVEL,
                            help="Set the logging level. Default: {0!s}".format(DEFAULT_LOG_LEVEL))
        parser.add_argument('-v', '--version',
                            action='version',
                            version=__version__)
        parser.add_argument('-V', '--verbose',
                            action='store_true',
                            default=False,
                            help='Give verbose output. Default: {0!s}'.format(VERBOSE))
        log.info("{0!s} Started".format(parser.prog))
        args = parser.parse_args()
        _set_log_level(log,args.log_level)        
        if args.debug:
            _set_log_level(log_fh,"DEBUG")
            _set_log_level(log,"DEBUG")
        elif args.verbose:
            _set_log_level(log_fh,"INFO")
            _set_log_level(log,"INFO")
        main(args)
        _set_log_level(log,"INFO")
        log.info("{0!s} Ended.  Elapsed time {1:0.2f} sec".format(parser.prog,time.time() - start_time))
        sys.exit(0)
    except KeyboardInterrupt as e:  # Ctrl-C
        raise e
    except SystemExit as e:  # sys.exit()
        raise e
    except Exception as e:
        print('ERROR, UNEXPECTED EXCEPTION')
        print(str(e))
        traceback.print_exc()
        os._exit(1)
