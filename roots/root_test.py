#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Test bisection routine
"""
__version__="0.0.1"
__author__="Jim Hefferon"
__license__="GPL3"

import sys
import os
import traceback
import argparse
import time
import unittest
import root1
import math

# Global variables spare me from putting them in the call of each fcn.
VERBOSE = False
DEBUG = False

class JHException(Exception):
    pass

def warn(s):
    t = 'WARNING: '+s+"\n"
    sys.stderr.write(t)
    sys.stderr.flush()

def error(s):
    t = 'ERROR! '+s
    sys.stderr.write(t)
    sys.stderr.flush()
    sys.exit(10)

import logging
# create file handler which logs even debug messages
log = logging.getLogger()
# Potential logging levels: DEBUG | INFO | WARNING | ERROR | CRITICAL
if DEBUG:
    log.setLevel(logging.DEBUG)  
else:
    log.setLevel(logging.ERROR)  # DEBUG | INFO | WARNING | ERROR | CRITICAL
formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - Line: %(lineno)d\n%(message)s')
sh = logging.StreamHandler()
sh.setLevel(logging.ERROR)
sh.setFormatter(formatter)
log.addHandler(sh)
fh = logging.FileHandler(os.path.abspath(os.path.join(
    os.path.dirname(__file__), 
    os.path.basename(__file__).rstrip('.py') + '.log'
)))
fh.setLevel(logging.ERROR)
fh.setFormatter(formatter)
log.addHandler(fh)

# ==============================================
class BisectionTestCase(unittest.TestCase):
    """Tests."""

 # make routine refuse to run if f(a) has same sign as f(b)
 #   def test1(self):
  #      """Do the dumbest possible thing"""
   #     def f(x):
    #        return x**2-2*x+1
     #   
      #  c, i = root1.bisection(f,0,2,0.001)
       # self.assertAlmostEqual(c,1,3)

    def test_distinct_roots(self):
        """Test (x+1)(x-1)"""
        def f(x):
            return x**2-1
        
        c, i = root1.bisection(f,0,2,0.001)
        self.assertAlmostEqual(c,1,3)
        c, i = root1.bisection(f,-2,0,0.001)
        self.assertAlmostEqual(c,-1,3)

    def test_irrational_roots(self):
        """Test root 2"""
        def f(x):
            return x**2-2
        
        c, i = root1.bisection(f,0,2,0.0001)
        self.assertAlmostEqual(c,math.sqrt(2),3)

    def test_sin_roots(self):
        """Test sine function"""
        def f(x):
            return math.sin(x)
        
        c, i = root1.bisection(f,3,4,0.0001)
        self.assertAlmostEqual(c,math.pi,3)

    def test_transcendental_function(self):
        """Test the two function values are the same sign"""
        def f(x):
            return x**2-1
        
        c, i = root1.bisection(f,2,3,0.0001)
        self.assertIsNone(c)
        c, i = root1.bisection(f,-2,2,0.0001)
        self.assertIsNone(c)

    def test_small_function(self):
        """Test the two function values are the same sign"""
        def f(x):
            return x**2-0.01
        
        c, i = root1.bisection(f,0,1,0.0001)
        self.assertAlmostEqual(c,0.1,3)


    def test_large_roots(self):
        """Test large roots"""
        def f(x):
            return math.sin(x)
        k = 10E6
        c, i = root1.bisection(f,k,k+7,0.0001)
        # see if c is a multiple of pi
        j = min(c % math.pi, -(-c % math.pi))
        self.assertAlmostEqual(j,0,2)




    
        

# ===========================================================
def main(args):
    unittest.main()

# ===========================================================
if __name__ == '__main__':
    try:
        start_time = time.time()
        # Parser: See http://docs.python.org/dev/library/argparse.html
        parser = argparse.ArgumentParser(description=__doc__+
                                         "\n"+__author__
                                         +" version: "+__version__
                                         +" license: "+__license__)
        parser.add_argument('-D', '--debug', action='store_true', default=DEBUG, help='run debugging code')
        parser.add_argument('-v', '--version', action='version', version=__version__)
        parser.add_argument('-V', '--verbose', action='store_true', default=False, help='verbose output')
        args = parser.parse_args()
        if args.debug:
            fh.setLevel(logging.DEBUG)
            log.setLevel(logging.DEBUG)
        elif args.verbose:
            fh.setLevel(logging.INFO)
            log.setLevel(logging.INFO)
        log.info("%s Started" % parser.prog)
        main(args)
        log.info("%s Ended" % parser.prog)
        log.info("Total running time in seconds: %0.2f" % (time.time() - start_time))
        sys.exit(0)
    except KeyboardInterrupt as e:  # Ctrl-C
        raise e
    except SystemExit as e:  # sys.exit()
        raise e
    except Exception as e:
        print('ERROR, UNEXPECTED EXCEPTION')
        print(str(e))
        traceback.print_exc()
        os._exit(1)
